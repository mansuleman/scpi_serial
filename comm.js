// defining our serial port
var serialport = require("serialport");
var SerialPort = serialport.SerialPort;

// this is a SCPI command
var scpi_command = process.argv[2];

// below is my specific arduino, can be changed to the desired one
var serialPort = new SerialPort("/dev/cu.usbserial-A9CJN5DX", {
// Arduino's baud-rate
  baudrate: 9600,
  dataBits: 8,
  parity: 'none',
  stopBits: 1,
  flowControl: false,
// this is the parser to read data
  parser: serialport.parsers.readline("\r")
});

// openining connection
serialPort.on("open", function () {
  console.log('open');

// sending data
  serialPort.on('data', function(data) {
      result = data.trim();
// receiving data as callback
      console.log('data received: ' + result);
// we get at least some response from Arduino
      if (result !== '') {
          console.log('scpi command successful');
      }
      else {
          console.log('scpi command not successful');
      }
  });

// below part of code is needed to get rid of the reset
  setTimeout(function() {
      console.log("waiting...");
    serialPort.write(scpi_command, function(err, results) {
      console.log('err ' + err);
      console.log('results ' + results);
    });
  }, 3000);
  
  serialPort.on('error', function (err) {
      console.error("error", err);
  });
});
