# README #


### What is this ###

* Uses node serial port (https://github.com/voodootikigod/node-serialport) to organize communication with the modules
* Gets SCPI command as an argument and sends to the module
* Prints out the response from the module
* Implemented as a single node file for simplicity and speed 


### How do I use this? ###

* Install node js
* Install node serial port
* Run with SCPI command as an argument

```
#!javascript

 node comm.js *IDN? 
```